#include "stdafx.h"
#include "CppUnitTest.h"

#include "UnusedPieces.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestQwirkle
{
	TEST_CLASS(UnusedPiecesTests)
	{
	public:

		TEST_METHOD(TestingGenerateNumberOfPieces)
		{
			UnusedPieces bag;
			Assert::IsTrue(bag.getPoolSize() == 108);
		}

		TEST_METHOD(TestGetRandomPiece)
		{
			UnusedPieces bag;
			Piece piece = bag.PickPiece();
			Assert::IsTrue(piece.GetColor() != Piece::Color::Red
				&& piece.GetShape() != Piece::Shape::Diamond);
		}
	};
}