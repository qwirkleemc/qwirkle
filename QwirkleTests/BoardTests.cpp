#include "stdafx.h"
#include "CppUnitTest.h"

#include "Board.h"
#include "HumanPlayer.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestQwirkle
{
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(DefaultConstructorEmptyBoard)
		{
			Board board;
			for (int line = 0; line < Board::k_maxHeight; line++)
				for (int column = 0; column < Board::k_maxWidth; column++)
					if (board[Board::Position(line, column)].GetColor() != Piece::Color::None)
						Assert::Fail();
		}

		TEST_METHOD(SetGetAtOneOne)
		{
			Board board;
			Piece piece;
			Board::Position position(1, 1);
			board.placePiece(piece, position);

			Assert::IsTrue(board[position].GetColor() != Piece::Color::None);
		}

		TEST_METHOD(CheckFirstPiecePlacedIsInMiddle)
		{
			Board board;
			HumanPlayer player;
			UnusedPieces bag;
			player.fillCurrentPieces(bag);
			Board::Position position(1, 1);
			player.PlacePiece(player.getPiece(1), board, position);
			if (board[Board::Position(Board::k_maxHeight / 2, Board::k_maxWidth / 2)].GetColor() == Piece::Color::None)
				Assert::Fail();
		}

		TEST_METHOD(GetAtOneOneConst)
		{
			const Board board;
			Board::Position position{ 1, 1 };

			Assert::IsFalse(board[position].GetColor() != Piece::Color::None);
		}

		TEST_METHOD(GetAtOneMinusOne)
		{
			Board board;
			Board::Position position(1, -1);

			Assert::ExpectException<const char*>([&]()
			{
				board[position];
			});
		}

		TEST_METHOD(CheckQwirkleLeft)
		{
			Board board;
			Piece piece1(Piece::Color::Blue, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			Piece piece3(Piece::Color::Blue, Piece::Shape::Plus);
			Piece piece4(Piece::Color::Blue, Piece::Shape::Square);
			Piece piece5(Piece::Color::Blue, Piece::Shape::Star);
			Piece piece6(Piece::Color::Blue, Piece::Shape::Sun);
			board.placePiece(piece1, Board::Position(0, 0));
			board.placePiece(piece2, Board::Position(0, 1));
			board.placePiece(piece3, Board::Position(0, 2));
			board.placePiece(piece4, Board::Position(0, 3));
			board.placePiece(piece5, Board::Position(0, 4));
			board.placePiece(piece6, Board::Position(0, 5));
			Assert::IsTrue(board.isQwirkle(Board::Position(0, 5), board, piece6));
		}

		TEST_METHOD(CheckQwirkleRight)
		{
			Board board;
			Piece piece1(Piece::Color::Blue, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			Piece piece3(Piece::Color::Blue, Piece::Shape::Plus);
			Piece piece4(Piece::Color::Blue, Piece::Shape::Square);
			Piece piece5(Piece::Color::Blue, Piece::Shape::Star);
			Piece piece6(Piece::Color::Blue, Piece::Shape::Sun);
			board.placePiece(piece1, Board::Position(0, 0));
			board.placePiece(piece2, Board::Position(0, 1));
			board.placePiece(piece3, Board::Position(0, 2));
			board.placePiece(piece4, Board::Position(0, 3));
			board.placePiece(piece5, Board::Position(0, 4));
			board.placePiece(piece6, Board::Position(0, 5));
			Assert::IsTrue(board.isQwirkle(Board::Position(0, 0), board, piece1));
		}

		TEST_METHOD(CheckQwirkleUp)
		{
			Board board;
			Piece piece1(Piece::Color::Blue, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			Piece piece3(Piece::Color::Blue, Piece::Shape::Plus);
			Piece piece4(Piece::Color::Blue, Piece::Shape::Square);
			Piece piece5(Piece::Color::Blue, Piece::Shape::Star);
			Piece piece6(Piece::Color::Blue, Piece::Shape::Sun);
			board.placePiece(piece1, Board::Position(0, 0));
			board.placePiece(piece2, Board::Position(1, 0));
			board.placePiece(piece3, Board::Position(2, 0));
			board.placePiece(piece4, Board::Position(3, 0));
			board.placePiece(piece5, Board::Position(4, 0));
			board.placePiece(piece6, Board::Position(5, 0));
			Assert::IsTrue(board.isQwirkle(Board::Position(5, 0), board, piece6));
		}

		TEST_METHOD(CheckQwirkleDown)
		{
			Board board;
			Piece piece1(Piece::Color::Blue, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			Piece piece3(Piece::Color::Blue, Piece::Shape::Plus);
			Piece piece4(Piece::Color::Blue, Piece::Shape::Square);
			Piece piece5(Piece::Color::Blue, Piece::Shape::Star);
			Piece piece6(Piece::Color::Blue, Piece::Shape::Sun);
			board.placePiece(piece1, Board::Position(0, 0));
			board.placePiece(piece2, Board::Position(1, 0));
			board.placePiece(piece3, Board::Position(2, 0));
			board.placePiece(piece4, Board::Position(3, 0));
			board.placePiece(piece5, Board::Position(4, 0));
			board.placePiece(piece6, Board::Position(5, 0));
			Assert::IsTrue(board.isQwirkle(Board::Position(0, 0), board, piece1));
		}

		TEST_METHOD(isNotQwirkle)
		{
			Board board;
			Piece piece1(Piece::Color::Green, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			Piece piece3(Piece::Color::Blue, Piece::Shape::Plus);
			Piece piece4(Piece::Color::Blue, Piece::Shape::Square);
			Piece piece5(Piece::Color::Blue, Piece::Shape::Star);
			Piece piece6(Piece::Color::Blue, Piece::Shape::Sun);
			board.placePiece(piece1, Board::Position(0, 0));
			board.placePiece(piece2, Board::Position(1, 0));
			board.placePiece(piece3, Board::Position(2, 0));
			board.placePiece(piece4, Board::Position(3, 0));
			board.placePiece(piece5, Board::Position(4, 0));
			board.placePiece(piece6, Board::Position(5, 0));
			Assert::IsFalse(board.isQwirkle(Board::Position(0, 0), board, piece1));
		}

		TEST_METHOD(pieceIsNotNearOtherPieces)
		{
			Board board;
			Assert::IsFalse(board.isNearOtherPieces(Board::Position(10, 10), board));
		}

		TEST_METHOD(pieceIsNearOtherPieces)
		{
			Board board;
			Piece piece(Piece::Color::Green, Piece::Shape::Circle);
			board.placePiece(piece, Board::Position(10, 10));
			Assert::IsTrue(board.isNearOtherPieces(Board::Position(10, 11), board));
		}

		TEST_METHOD(notAbleToPlacePieceBetweenTwoOthersUpAndDown)
		{
			Board board;
			Piece piece1(Piece::Color::Green, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			board.placePiece(piece1, Board::Position(10, 10));
			board.placePiece(piece2, Board::Position(12, 10));
			Assert::IsTrue(board.validatePositionTop(Board::Position(11, 10)));
		}

		TEST_METHOD(notAbleToPlacePieceBetweenTwoOthersLeftAndRight)
		{
			Board board;
			Piece piece1(Piece::Color::Green, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			board.placePiece(piece1, Board::Position(10, 10));
			board.placePiece(piece2, Board::Position(10, 12));
			Assert::IsTrue(board.validatePositionLeft(Board::Position(10, 11)));
		}
	};
}