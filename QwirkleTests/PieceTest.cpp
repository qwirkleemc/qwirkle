#include "stdafx.h"
#include "CppUnitTest.h"

#include "Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestQwirkle
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(TestingPieceConstructorOne)
		{
			Piece piece(Piece::Color::Orange, Piece::Shape::Circle);
			Assert::IsTrue(piece.GetColor() == Piece::Color::Orange);
			Assert::IsTrue(piece.GetShape() == Piece::Shape::Circle);
		}

		TEST_METHOD(TestingPieceConstructorTwo)
		{
			Piece pieceAux(Piece::Color::Orange, Piece::Shape::Circle);
			Piece piece(pieceAux);
			Assert::IsTrue(piece.GetColor() == Piece::Color::Orange);
			Assert::IsTrue(piece.GetShape() == Piece::Shape::Circle);
		}

		TEST_METHOD(TestEqualsOperator)
		{
			Piece piece1(Piece::Color::Green, Piece::Shape::Circle);
			Piece piece2(Piece::Color::Blue, Piece::Shape::Diamond);
			piece1 = piece2;
			Assert::IsTrue(piece1.GetColor() == Piece::Color::Blue);
			Assert::IsTrue(piece1.GetShape() == Piece::Shape::Diamond);
		}
	};
}