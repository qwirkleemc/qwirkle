#include "stdafx.h"
#include "CppUnitTest.h"

#include "HumanPlayer.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestQwirkle
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(TestPlayerConstructor)
		{
			UnusedPieces bag;
			HumanPlayer player(1, "Bob", 20, bag);
			Assert::IsTrue(player.getId() != NULL && player.getAge() != NULL);
			Assert::IsFalse(player.getName().empty());
			Assert::IsFalse(player.getCurrentPieces().empty());
		}
	};
}