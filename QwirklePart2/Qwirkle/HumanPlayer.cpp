#include "HumanPlayer.h"
#include "Helpers.h"
#include <fstream>
#include "../LoggingDLL/Logging.h"
HumanPlayer::HumanPlayer() :
	m_name(" ")
{
	id = 0;
	points = 0;
}

HumanPlayer::HumanPlayer(uint16_t arg_id, std::string name, uint16_t age, UnusedPieces & bag) :
	m_name(name),
	m_age(age)
{
	id = arg_id;
	points = 0;
	for (uint16_t index = 0; index < 6; index++)
		currentPieces.push_back(bag.PickPiece());
}


HumanPlayer::~HumanPlayer()
{
}

bool HumanPlayer::PlacePiece(Piece&& piece, Board& board, Position pos)
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Placing piece..", Logger::Level::Info);
	uint16_t line = UINT16_MAX;
	uint16_t column = UINT16_MAX;
	Position position = pos;
	line = pos.first;
	column = pos.second;
	if (board.placedPiecesNumber == 0)
	{
		position.first = board.k_maxHeight / 2;
		position.second = board.k_maxWidth / 2;
		board.placePiece(piece, position);
		points += 1;
		return true;
	}

	if (!board.validatePosition(piece, position) || board.isNearOtherPieces(position, board) == false)
	{
		//std::cout << "Piece cannot be placed here.";
		currentPieces.push_back(piece);
		log.log("Pozitia nu a fost valida", Logger::Level::Info);
		return false;
	}
	else
	{
		int aux = board.findViablePosition(position, board);
		if (aux != -1)
		{
			board.viablePositions.erase(board.viablePositions.begin() + aux);
			board.addViablePositions(position, board);
		}
		board.placePiece(piece, position);
		log.log("Pozitia a fost valida", Logger::Level::Info);
		if (board.validatePositionLeft(position))
		{
			points += 2;
			board.addViablePositions(position, board);

		}
		else if (board.validatePositionTop(position))
		{
			points += 2;
			board.addViablePositions(position, board);
		}
		else if (board.isQwirkle(position, board, piece))
			points += 6;
		else
			points += 1;
	}
	return true;
}

uint16_t HumanPlayer::getAge() {
	return m_age;
}

Piece HumanPlayer::getPiece(uint16_t index) {
	Piece piece = currentPieces.at(index);
	currentPieces.erase(currentPieces.begin() + index);

	return piece;
}

std::string HumanPlayer::getName()
{
	return m_name;
}

uint16_t HumanPlayer::checkMax() {
	uint16_t count = 1, max = 0;
	std::sort(currentPieces.begin(), currentPieces.end(), [](const Piece & a, const Piece & b) -> bool
	{
		return a.GetColor() > b.GetColor();
	});
	for (uint16_t index = 1; index < currentPieces.size(); index++) {
		if (currentPieces[index].GetColor() == currentPieces[index - 1].GetColor())
			count++;
		else
			count = 1;
		if (count > max)
			max = count;
	}
	count = 1;
	std::sort(currentPieces.begin(), currentPieces.end(), [](const Piece & a, const Piece & b) -> bool
	{
		return a.GetShape() > b.GetShape();
	});
	for (uint16_t index = 1; index < currentPieces.size(); index++) {
		if (currentPieces[index].GetShape() == currentPieces[index - 1].GetShape())
			count++;
		else
			count = 1;
		if (count > max)
			max = count;
	}

	return max;
}

std::ostream& operator<<(std::ostream& os, const HumanPlayer& player)
{
	return os << player.m_name;
}
