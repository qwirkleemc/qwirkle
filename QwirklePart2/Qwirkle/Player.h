#pragma once
#include<String>
#include "UnusedPieces.h"
#include "Board.h"
class Player
{

public:
	using Position = std::pair<uint8_t, uint8_t>;

	Player();

	virtual bool PlacePiece(Piece&& piece, Board& board, Position pos) = 0;

	void changePieces(std::vector<uint16_t> indexes, UnusedPieces & unusedPieces);
	void fillCurrentPieces(UnusedPieces& unusedPieces);
	std::vector<Piece> getCurrentPieces();

	int getPoints();
	uint16_t getId();
protected:
	uint16_t id;
	uint16_t points;
	std::vector<Piece> currentPieces;

};