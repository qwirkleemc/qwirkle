#pragma once
#include "Piece.h"
#include<vector>
using namespace std;

class UnusedPieces
{
public:
	UnusedPieces();
	Piece PickPiece();
	void EmplacePiece(Piece piece);
	bool isEmpty();
	friend std::ostream & operator << (std::ostream& os, const UnusedPieces& unusedPieces);
	int getPoolSize();
private:
	void GeneratePieces();

private:
	vector<Piece> m_pool;
	int numberOfPieces = 107;
};

