#pragma once
#include "HumanPlayer.h"

class PlayerView
{
public:
	PlayerView();
	~PlayerView();

	static void showName(std::ostream& os, HumanPlayer player);
	static void showScore(std::ostream& os, HumanPlayer player);
	static void showAge(std::ostream& os, HumanPlayer player);
	static void showPieces(std::ostream& os, HumanPlayer player);

};

