#pragma once
#include "Piece.h"
#include<array>
#include "Board.h"

class PieceView
{
public:
	PieceView();
	~PieceView();

	static void showColor(std::ostream& os, Piece piece);
	static void showShape(std::ostream& os, Piece piece);
private:
	static std::array<std::pair<std::string, std::string>, 6> pieceTypes;
};