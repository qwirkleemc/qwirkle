#include "Piece.h"
#include <vector>
#include<assert.h>

Piece::Piece()
{

}

Piece::Piece(Color color, Shape shape) :
	m_color(color),
	m_shape(shape)
{

}

Piece::Piece(const Piece & other)
{
	*this = other;
}

Piece::Piece(Piece && other)
{
	*this = std::move(other);
}


Piece::~Piece()
{
}

Piece& Piece::operator= (const Piece& other)
{

	m_color = other.m_color;
	m_shape = other.m_shape;

	return *this;
}

Piece& Piece::operator= (Piece&& other)
{
	m_color = other.m_color;
	m_shape = other.m_shape;


	new(&other) Piece;

	return *this;
}

Piece& Piece::operator&=(const Piece & other)
{
	if (this->m_color != other.m_color)
		this->m_color = Color::None;


	if (this->m_shape != other.m_shape)
		this->m_shape = Shape::None;

	return *this;
}

Piece Piece::operator&(Piece other) const
{
	other &= *this;
	return other;
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}

std::ostream & operator<<(std::ostream & os, const Piece & piece)
{
	return os <<
		static_cast<int>(piece.m_color) <<
		static_cast<int>(piece.m_shape);
}