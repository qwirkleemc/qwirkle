#include<iostream>
#include "Board.h"
#include "HumanPlayer.h"
#include "Game.h"
#include "PlayerView.h"
#include "../LoggingDLL/Logging.h"
#include <fstream>
#include <time.h>
//
int main()
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Started Application..", Logger::Level::Info);
	srand(time(NULL));
	Game game;
	game.play(cin, cout);

	return 0;
}