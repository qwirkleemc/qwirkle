#include "Board.h"
#include "../LoggingDLL/Logging.h"
#include <fstream>
using namespace std;

const char kEmptyBoardCell[] = "__";
const Piece noPiece(Piece::Color::None, Piece::Shape::None);

Board::Board()
{
	for (int line = 0; line < Board::k_maxHeight; line++)
		for (int column = 0; column < Board::k_maxWidth; column++)
			m_pieces[line][column] = noPiece;
}

const Piece & Board::operator[](const Position & pos) const
{
	const auto&[line, column] = pos;

	if (line >= k_maxHeight || column >= k_maxWidth)
		throw "Board index out of bound.";

	return m_pieces[line][column];
}

std::ostream & operator<<(std::ostream & os, const Board & board)
{
	Board::Position position;
	auto&[line, column] = position;
	for (line = 0; line < Board::k_maxHeight; line++)
	{
		for (column = 0; column < Board::k_maxWidth; column++)
		{
			if (board[position].GetColor() != noPiece.GetColor() && board[position].GetShape() != noPiece.GetShape())
				os << board[position];
			else
				os << kEmptyBoardCell;
			os << ' ';
		}
		os << std::endl;
	}
	return os;
}

//checks if the positions to the left and the right of where the player wants to place a piece are empty or not
bool Board::validatePositionLeft(const Position pos)
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Validate position left...", Logger::Level::Info);
	uint16_t line = pos.first;
	uint16_t column = pos.second;
	if (m_pieces[line][column - 1].GetColor() != noPiece.GetColor()
		&& m_pieces[line][column - 1].GetShape() != noPiece.GetShape()
		&& m_pieces[line][column + 1].GetColor() != noPiece.GetColor()
		&& m_pieces[line][column + 1].GetShape() != noPiece.GetShape())
	{
		return true;
		log.log("The position is valid", Logger::Level::Info);
	}
	else
	{
		return false;
		log.log("The position is not valid", Logger::Level::Error);
	}
	return true;
}

//checks if the positions to the top and under where the player wants to place a piece are empty or not
bool Board::validatePositionTop(const Position pos)
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Validate position top...", Logger::Level::Info);
	uint16_t line = pos.first;
	uint16_t column = pos.second;
	if (m_pieces[line - 1][column].GetColor() != noPiece.GetColor()
		&& m_pieces[line - 1][column].GetShape() != noPiece.GetShape()
		&& m_pieces[line + 1][column].GetColor() != noPiece.GetColor()
		&& m_pieces[line + 1][column].GetShape() != noPiece.GetShape())
	{
		return true;
		log.log("The position is valid", Logger::Level::Info);
	}
	else
	{
		return false;
		log.log("The position is not valid", Logger::Level::Error);
	}
	return true;
}

bool Board::validatePosition(Piece piece, const Position pos)
{
	uint16_t line = pos.first;
	uint16_t column = pos.second;

	//out of bounds
	if (line - 1 < 0 || column - 1 < 0 || line + 1 > k_maxHeight || column + 1 > k_maxWidth)
		return false;
	if (line > k_maxHeight)
		return false;
	else if (column > k_maxWidth)
		return false;
	//position occupied already
	else if (m_pieces[line][column].GetColor() != noPiece.GetColor() && m_pieces[line][column].GetShape() != noPiece.GetShape())
		return false;

	const vector<pair<int16_t, int16_t>> directions = { make_pair(0, -1), make_pair(1, 0), make_pair(0, 1), make_pair(-1, 0) };
	for (auto && direction : directions)
		if (m_pieces[line + direction.first][column + direction.second].GetColor() != noPiece.GetColor())
		{
			bool type;
			if (m_pieces[line + direction.first][column + direction.second].GetColor() == piece.GetColor()
				&& m_pieces[line + direction.first][column + direction.second].GetShape() == piece.GetShape())
				return false;
			else if (m_pieces[line + direction.first][column + direction.second].GetColor() == piece.GetColor())
				type = 0;
			else if (m_pieces[line + direction.first][column + direction.second].GetShape() == piece.GetShape())
				type = 1;
			else
				return false;

			uint16_t index = 2;
			while (column - index >= 0 && m_pieces[line + direction.first][column + direction.second].GetColor() != noPiece.GetColor())
				if (m_pieces[line + direction.first][column + direction.second].GetColor() == piece.GetColor()
					&& m_pieces[line + direction.first][column + direction.second].GetShape() == piece.GetShape())
					return false;
				else if (type == 1 && m_pieces[line + direction.first][column + direction.second].GetColor() == piece.GetColor())
					return false;
				else if (type == 0 && m_pieces[line + direction.first][column + direction.second].GetShape() == piece.GetShape())
					return false;
				else
					index++;
		}

	return true;
}

//checks if there are other pieces near the position
bool Board::isNearOtherPieces(const Position & p, const Board & board)
{
	if (board[Position(p.first - 1, p.second)].GetColor() != noPiece.GetColor())
		return true;
	if (board[Position(p.first + 1, p.second)].GetColor() != noPiece.GetColor())
		return true;
	if (board[Position(p.first, p.second - 1)].GetColor() != noPiece.GetColor())
		return true;
	if (board[Position(p.first, p.second + 1)].GetColor() != noPiece.GetColor())
		return true;
	return false;
}

bool Board::isQwirkle(const Position & p, const Board & board, Piece piece)
{
	vector<Piece> checkQwirkle;
	int qwirkleCount = 0;
	bool foundQwirkle = false;
	//checks left of position
	for (int index = 1; index < 6; index++)
		if (p.second - index >= 0)
			checkQwirkle.push_back(board[Position(p.first, p.second - index)]);
	if (checkQwirkle.size() == 5)
	{
		//checks colors
		for (int index = 0; index < 5; index++)
			if (checkQwirkle[index].GetColor() == piece.GetColor())
				qwirkleCount++;
		if (qwirkleCount == 5)
			foundQwirkle = true;
		qwirkleCount = 0;
		//checks shapes
		for (int index = 0; index < 5; index++)
			if (checkQwirkle[index].GetShape() == piece.GetShape())
				qwirkleCount++;
		if (qwirkleCount == 5)
			foundQwirkle = true;
		qwirkleCount = 0;
	}
	checkQwirkle.clear();
	//checks right of position
	for (int index = 1; index < 6; index++)
		if (p.second + index < k_maxWidth)
			checkQwirkle.push_back(board[Position(p.first, p.second + index)]);
	if (checkQwirkle.size() == 5)
	{
		//checks colors
		for (int index = 0; index < 5; index++)
			if (checkQwirkle[index].GetColor() == piece.GetColor())
				qwirkleCount++;
		if (qwirkleCount == 5)
			foundQwirkle = true;
		qwirkleCount = 0;
		//checks shapes
		for (int index = 0; index < 5; index++)
			if (checkQwirkle[index].GetShape() == piece.GetShape())
				qwirkleCount++;
		if (qwirkleCount == 5)
			foundQwirkle = true;
		qwirkleCount = 0;
	}
	checkQwirkle.clear();
	//checks upwards of position
	for (int index = 1; index < 6; index++)
		if (p.first - index >= 0)
			checkQwirkle.push_back(board[Position(p.first - index, p.second)]);
	if (checkQwirkle.size() == 5)
	{
		//checks colors
		for (int index = 0; index < 5; index++)
			if (checkQwirkle[index].GetColor() == piece.GetColor())
				qwirkleCount++;
		if (qwirkleCount == 5)
			foundQwirkle = true;
		qwirkleCount = 0;
		//checks shapes
		for (int index = 0; index < 5; index++)
			if (checkQwirkle[index].GetShape() == piece.GetShape())
				qwirkleCount++;
		if (qwirkleCount == 5)
			foundQwirkle = true;
		qwirkleCount = 0;
	}
	checkQwirkle.clear();
	//checks downwards of position
	for (int index = 1; index < 6; index++)
		if (p.first + index < k_maxHeight)
			checkQwirkle.push_back(board[Position(p.first + index, p.second)]);
	if (checkQwirkle.size() == 5)
	{
		//checks colors
		for (int index = 0; index < 5; index++)
			if (checkQwirkle[index].GetColor() == piece.GetColor())
				qwirkleCount++;
		if (qwirkleCount == 5)
			foundQwirkle = true;
		qwirkleCount = 0;
		//checks shapes
		for (int index = 0; index < 5; index++)
			if (checkQwirkle[index].GetShape() == piece.GetShape())
				qwirkleCount++;
		if (qwirkleCount == 5)
			foundQwirkle = true;
		qwirkleCount = 0;
	}
	checkQwirkle.clear();
	return foundQwirkle;
}

void Board::addViablePositions(const Position &p, const Board & board)
{
	if (p.first - 1 > 0 && board[Position(p.first - 1, p.second)].GetColor() == noPiece.GetColor())
	{
		viablePositions.push_back(Position(p.first - 1, p.second));
	}
	if (p.first + 1 < k_maxWidth && board[Position(p.first + 1, p.second)].GetColor() == noPiece.GetColor())
	{
		viablePositions.push_back(Position(p.first + 1, p.second));
	}
	if (p.second - 1 > 0 && board[Position(p.first, p.second - 1)].GetColor() == noPiece.GetColor())
	{
		viablePositions.push_back(Position(p.first, p.second - 1));
	}
	if (p.second + 1 < k_maxHeight && board[Position(p.first, p.second + 1)].GetColor() == noPiece.GetColor())
	{
		viablePositions.push_back(Position(p.first, p.second + 1));
	}

}

int Board::findViablePosition(const Position &position, const Board & board)
{
	for (int index = 0; index < viablePositions.size(); index++)
	{
		if (viablePositions.at(index).first == position.first && viablePositions.at(index).second == position.second)
			return index;
	}
	return -1;
}

uint16_t Board::getRemainingPiecesNumber()
{
	return 108 - placedPiecesNumber;
}

void Board::placePiece(Piece piece, const Position pos)
{
	auto&[line, column] = pos;
	m_pieces[line][column] = piece;
	placedPiecesNumber++;
}