#pragma once
#include <iostream>
#include<vector>
class Piece
{
public:
	enum class Color : uint16_t
	{
		Red,
		Orange,
		Blue,
		Purple,
		Green,
		Yellow,
		None
	};
	enum class Shape : uint16_t
	{
		Diamond,
		Square,
		Circle,
		Plus,
		Star,
		Sun,
		None
	};


public:
	Piece();
	Piece(Color color, Shape shape);
	Piece(const Piece &other);
	Piece(Piece && other);
	~Piece();

	Piece & operator = (const Piece & other);
	Piece & operator = (Piece && other);

	Piece & operator &= (const Piece & other);
	Piece operator & (Piece other) const;

	Color GetColor() const;
	Shape GetShape() const;

	friend std::ostream& operator<<(std::ostream& os, const Piece& piece);

private:
	Color m_color : 16;
	Shape m_shape : 16;
};