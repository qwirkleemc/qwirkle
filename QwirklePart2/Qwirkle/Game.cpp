#include "Game.h"
#include "UnusedPieces.h"
#include <string>
#include "PlayerView.h"
#include "../LoggingDLL/Logging.h"
#include <fstream>
#include <queue>

Game::Game()
{
}


Game::~Game()
{
}

HumanPlayer Game::setHighScore(std::vector< HumanPlayer > users)
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Calculate score..", Logger::Level::Info);
	HumanPlayer maxUser = users.front();
	for (auto && user : users)
		if (user.getPoints() > maxUser.getPoints())
		{
			maxUser = user;
			log.log("This player has the highest score", Logger::Level::Info);
		}

	return maxUser;
}

HumanPlayer Game::whoIsFirst(std::vector< HumanPlayer > users)
{
	std::vector<HumanPlayer> maxUsers;
	uint8_t countMax = 0;
	for (auto && user : users)
		if (user.checkMax() == countMax)
			maxUsers.push_back(user);
		else if (user.checkMax() > countMax)
		{
			maxUsers.clear();
			maxUsers.push_back(user);
			countMax = user.checkMax();
		}
	if (maxUsers.size() == 1)
		return maxUsers.front();
	else
	{
		HumanPlayer playerMin = users.front();
		for (auto&& user : users)
			if (user.getAge() < playerMin.getAge())
				playerMin = user;
		return playerMin;
	}
}

void Game::play(std::istream& in, std::ostream& os)
{
	std::vector<HumanPlayer> users;
	Board board;
	UnusedPieces bag;
	int numberOfPlayers = 2;
	os << board << "\n\n\n";

	//Getting the player's name and age
	std::string name;
	uint16_t age;
	os << "Type in your name: ";
	in >> name;
	os << "Type your age: ";
	in >> age;
	HumanPlayer player(0, name, age, bag);
	users.push_back(player);
	HumanPlayer computer(1, "Computer", 100, bag);
	users.push_back(computer);

	//Main game, taking turns, taking in the player's options
	int userTurn = 0;  //the user whose turn is
	while (!bag.isEmpty())
	{
		//user's turn
		if (users[userTurn].getId() == 0)
		{
			os << "\nPlayer options: \n0 - End turn;\n1 - Place a piece;\n2 - Exchange pieces;\n3 - Let the computer place a piece for me;\n";
			os << "\nIt's " << users[userTurn].getName() << "'s turn.\n";
			PlayerView::showScore(os, users[userTurn]); os << endl;
			os << "\nPieces in hand: ";
			PlayerView::showPieces(os, users[userTurn]);
			int userChoice;
			os << "\nSelect an option: ";
			in >> userChoice;
			switch (userChoice)
			{
			case 0:
			{
				users[userTurn].fillCurrentPieces(bag);
				userTurn = 1;
				std::cout << board << std::endl;
				break;
			}
			case 1:
			{
				if (users[userTurn].getCurrentPieces().size() == 0)
					os << "\nYou do not have any pieces!";
				os << "\nPieces in hand: ";
				PlayerView::showPieces(os, users[userTurn]);
				os << "\nInsert positions (line, column): ";
				uint16_t i, j;
				in >> i >> j;
				uint16_t pieceID;
				os << "Which piece do you want to place? ";
				in >> pieceID;
				if (users[userTurn].getCurrentPieces().size() - 1 >= pieceID)
					if (users[userTurn].PlacePiece(users[userTurn].getPiece(pieceID), board, std::make_pair(i, j)))
						os << "\nSucces!\n";
					else
						os << "\nFailed!\n";
				else
					os << "\nInvalid piece!\n";
				std::cout << board << std::endl;
				break;
			}
			case 2:
			{
				os << "\nPieces in hand: ";
				PlayerView::showPieces(os, users[userTurn]);
				std::vector<uint16_t> piecesIds;
				os << "\nHow many pieces do you want to change? ";
				uint16_t no;
				in >> no;
				os << "\nChoose what pieces to change: ";
				for (uint16_t index = 0; index < no; index++)
				{
					uint16_t x;
					in >> x;
					piecesIds.push_back(x);
				}
				users[userTurn].changePieces(piecesIds, bag);
				break;
			}
			case 3:
			{
				PlayerView::showPieces(os, users[userTurn]);
				os << "\nWhich piece would you like the computer to try to place?";
				uint16_t pieceID;
				in >> pieceID;
				if (BFS(board, users[userTurn], 14, 14, pieceID) == true)
					os << "\nThe piece was placed by the CPU for you!";
				else
					os << "\nThat piece couldn't be placed by the computer!";
				break;
			}
			default:
			{
				os << "\nInvalid option!\n";
				break;
			}
			}
		}
		//computer's turn
		else if (users[userTurn].getId() == 1)
		{
			os << "\nIt's " << users[userTurn].getName() << "'s turn.\n";
			PlayerView::showScore(os, users[userTurn]); os << endl;

			if (board.placedPiecesNumber == 0)
			{
				users[userTurn].PlacePiece(users[userTurn].getPiece(0), board, std::make_pair(14, 14));
			}

			if (users[userTurn].getCurrentPieces().size() == 0)
			{
				users[userTurn].fillCurrentPieces(bag);
				userTurn = 0;
				std::cout << board << std::endl;
			}

			int pieceID = 0;
			vector<bool> pieceChecked(6, false);
			while (checkBoolVector(pieceChecked) != true && pieceID < 6 && users[userTurn].getCurrentPieces().size() > 0)
			{
				if (BFS(board, users[userTurn], 14, 14, pieceID) == true)
				{
					pieceChecked.erase(pieceChecked.begin() + pieceID);
					pieceID = 0;
				}
				else
				{
					pieceChecked[pieceID] = true;
					pieceID++;
				}
			}
			if (checkBoolVector(pieceChecked) == true)
			{
				users[userTurn].fillCurrentPieces(bag);
				userTurn = 0;
				std::cout << board << std::endl;
			}
		}
	}
	if (bag.isEmpty() && users[userTurn].getCurrentPieces().size() == 0)
	{
		if (users[0].getPoints() > users[1].getPoints())
			os << endl << users[0].getName() << " won the game!";
		else
			os << endl << users[1].getName() << " won the game!";
	}
}

bool Game::BFS(Board& board, HumanPlayer& computer, int posX, int posY, int pieceID)
{
	bool visited[28][28] = { false };
	queue<pair<int, int>> toVisit;

	int directionLine[] = { 1, 0, -1, 0 };
	int directionColumn[] = { 0, 1, 0, -1 };

	visited[posX][posY] = true;
	pair<int, int> node(posX, posY);
	toVisit.push(node);

	while (toVisit.empty() != true)
	{
		pair<int, int> current = toVisit.front();
		toVisit.pop();

		if (computer.PlacePiece(computer.getPiece(pieceID), board, std::make_pair(current.first, current.second)))
		{
			cout << "\nThe Computer has placed a piece!\n";
			std::cout << board << std::endl;
			return true;
		}

		for (int index = 0; index < 4; index++)
		{
			node = make_pair(current.first + directionLine[index], current.second + directionColumn[index]);

			if (node.first >= 0 && node.second >= 0 && node.first < 28 && node.second < 28)
				if (visited[node.first][node.second] == false && board.isNearOtherPieces(make_pair(node.first, node.second), board) == true)
				{
					visited[node.first][node.second] = true;
					toVisit.push(node);
				}
		}
	}
	return false;
}

bool Game::checkBoolVector(std::vector<bool> vector)
{
	for (int index = 0; index < vector.size(); index++)
		if (vector[index] == false)
			return false;
	return true;
}
