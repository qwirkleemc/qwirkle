#include "PlayerView.h"
#include "Helpers.h"
#include "PieceView.h"
#include "../LoggingDLL/Logging.h"
#include <fstream>

PlayerView::PlayerView()
{
}


PlayerView::~PlayerView()
{
}

void PlayerView::showName(std::ostream& os, HumanPlayer player)
{
	os << "Name of the player is " << player.getName();
}

void PlayerView::showScore(std::ostream& os, HumanPlayer player) 
{
	os << player.getName() << " has " << player.getPoints() << " points.";
}

void PlayerView::showAge(std::ostream& os, HumanPlayer player) 
{
	os << player.getName() << " has " << player.getAge() << " years.";
}

void PlayerView::showPieces(std::ostream& os, HumanPlayer player)
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Show the pieces..", Logger::Level::Info);
	for (auto&& piece : player.getCurrentPieces()) 
	{
		os << "(";
		PieceView::showColor(os, piece);
		log.log("Show the color", Logger::Level::Info);
		os << ',';
		PieceView::showShape(os, piece);
		log.log("Show the shape", Logger::Level::Info);
		os << ") ";
	}
	os << '\n';
}