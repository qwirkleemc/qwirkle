#include "Player.h"
#include "Piece.h"
#include "UnusedPieces.h"
#include "../LoggingDLL/Logging.h"
#include <fstream>
#include <random>
#include <time.h>

Player::Player()
{

}


void Player::changePieces(std::vector<uint16_t> indexes, UnusedPieces & unusedPieces)
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Changing piece..", Logger::Level::Info);
	for (auto&& index : indexes)
	{
		currentPieces[index] = unusedPieces.PickPiece();
		log.log("The piece has been changed", Logger::Level::Info);
	}
}

void Player::fillCurrentPieces(UnusedPieces & unusedPieces)
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Fill pieces..", Logger::Level::Info);
	uint16_t noPieces = currentPieces.size();
	if (noPieces == 6)
		return;
	while (noPieces < 6) {
		currentPieces.push_back(unusedPieces.PickPiece());
		noPieces++;
		log.log("The pieces have been added", Logger::Level::Info);
	}
}

int Player::getPoints()
{
	return points;
}

uint16_t Player::getId()
{
	return id;
}

std::vector<Piece> Player::getCurrentPieces() {
	return currentPieces;
}
