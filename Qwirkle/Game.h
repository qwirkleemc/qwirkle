#pragma once
#include<iostream>
#include<vector>
#include "HumanPlayer.h"
#include "Board.h"

class Game
{
public:
	Game();
	~Game();

	HumanPlayer setHighScore(std::vector<HumanPlayer> usersScores);
	HumanPlayer whoIsFirst(std::vector<HumanPlayer> users);
	void play(std::istream& in, std::ostream& os);


private:
	uint8_t high_score;
};