#pragma once

#include "Piece.h"

#include <array>
#include <optional>

class Board
{
public:

	static const size_t k_maxWidth = 28;
	static const size_t k_maxHeight = 28;
	static const size_t k_maxSize = k_maxWidth * k_maxHeight;
public:
	using Position = std::pair<uint16_t, uint16_t>;

public:
	Board();
	std::vector<Position> viablePositions;
	uint16_t placedPiecesNumber = 0;


	const Piece& operator[] (const Position& pos) const;

	friend std::ostream& operator << (std::ostream& os, const Board& board);
	void placePiece(Piece piece, const Position pos);

	bool validatePositionLeft(const Position pos);
	bool validatePositionTop(const Position pos);
	bool validatePosition(Piece piece, const Position pos);
	bool isNearOtherPieces(const Position &p, const Board& board);
	bool isQwirkle(const Position &p, const Board& board, Piece piece);
	int findViablePosition(const Position &position, const Board & board);
	void addViablePositions(const Position &position, const Board& board);
	uint16_t getRemainingPiecesNumber();

private:

	//std::array<std::optional<Piece>, k_maxSize> m_pieces;
	Piece m_pieces[k_maxHeight][k_maxHeight];
};