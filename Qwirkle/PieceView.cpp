#include "PieceView.h"
#include "Helpers.h"
#include <string>


PieceView::PieceView()
{
}


PieceView::~PieceView()
{
}

std::array<std::pair<std::string, std::string>, 6> PieceView::pieceTypes = {
   std::make_pair("Red", "Diamond"),
   std::make_pair("Orange", "Square"),
   std::make_pair("Blue", "Circle"),
   std::make_pair("Purple", "Plus"),
   std::make_pair("Green", "Star"),
   std::make_pair("Yellow", "Sun")
};

void PieceView::showColor(std::ostream& os, Piece piece) {
	os << pieceTypes[Helpers::as_integer(piece.GetColor())].first;
}

void PieceView::showShape(std::ostream& os, Piece piece) {
	os << pieceTypes[Helpers::as_integer(piece.GetShape())].second;
}