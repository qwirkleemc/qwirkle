#include "UnusedPieces.h"
#include <sstream>
#include <array>
#include <algorithm>
#include<random>
#include<time.h>
#include <chrono>
#include "Helpers.h"
using namespace std;


UnusedPieces::UnusedPieces()
{
	GeneratePieces();
}

Piece UnusedPieces::PickPiece()
{
	int randomPieceIndex = rand() % numberOfPieces + 0;
	Piece newPiece = m_pool.at(randomPieceIndex);
	m_pool.erase(m_pool.begin() + randomPieceIndex);
	numberOfPieces--;
	return newPiece;
}

void UnusedPieces::GeneratePieces()
{
	const uint8_t kPermutationPoolSize = 12;
	array<uint8_t, kPermutationPoolSize> permutationPool = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5 };
	for (int indexColor = 0; indexColor < 6; indexColor++)
		for (int indexShape = 0; indexShape < 6; indexShape++)
		{
			EmplacePiece(Piece(
				static_cast<Piece::Color>(indexColor),
				static_cast<Piece::Shape>(indexShape)
			));
			EmplacePiece(Piece(
				static_cast<Piece::Color>(indexColor),
				static_cast<Piece::Shape>(indexShape)
			));
			EmplacePiece(Piece(
				static_cast<Piece::Color>(indexColor),
				static_cast<Piece::Shape>(indexShape)
			));
		}
}


void UnusedPieces::EmplacePiece(Piece piece)
{
	m_pool.push_back(piece);
}


ostream & operator<<(ostream & os, const UnusedPieces & unusedPieces)
{
	if (!unusedPieces.m_pool.size())
		throw("The bag is empty");
	for (const auto&piece : unusedPieces.m_pool)
		os << "Color: " << Helpers::as_integer(piece.GetColor()) << ' ' << "Shape:" << Helpers::as_integer(piece.GetShape()) << '\n';
	return os;
}


bool UnusedPieces::isEmpty()
{
	if (m_pool.empty())
		return true;
	return false;
}

int UnusedPieces::getPoolSize()
{
	return m_pool.size();
}