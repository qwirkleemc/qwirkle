#pragma once
#include "Player.h"
#include "Board.h"
#include<string>
class HumanPlayer :
	public Player
{
public:
	HumanPlayer();
	HumanPlayer(uint16_t arg_id, std::string name, uint16_t age, UnusedPieces & bag);
	~HumanPlayer();

	virtual bool PlacePiece(Piece&& piece, Board& board, Position pos) override;
	uint16_t checkMax();

	uint16_t getAge();
	Piece getPiece(uint16_t index);
	std::string getName();

	friend std::ostream& operator << (std::ostream& os, const HumanPlayer& player);

private:
	std::string m_name;
	uint16_t m_age;
};