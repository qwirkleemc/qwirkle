#include "Game.h"
#include "UnusedPieces.h"
#include <string>
#include "PlayerView.h"
#include "../LoggingDLL/Logging.h"
#include <fstream>

Game::Game()
{
}


Game::~Game()
{
}

HumanPlayer Game::setHighScore(std::vector< HumanPlayer > users)
{
	std::ofstream of("app.log", std::ios::app);
	Logger log(of);
	log.log("Calculate score..", Logger::Level::Info);
	HumanPlayer maxUser = users.front();
	for (auto && user : users)
		if (user.getPoints() > maxUser.getPoints())
		{
			maxUser = user;
			log.log("This player has the highest score", Logger::Level::Info);
		}

	return maxUser;
}

HumanPlayer Game::whoIsFirst(std::vector< HumanPlayer > users)
{
	std::vector<HumanPlayer> maxUsers;
	uint8_t countMax = 0;
	for (auto && user : users)
		if (user.checkMax() == countMax)
			maxUsers.push_back(user);
		else if (user.checkMax() > countMax)
		{
			maxUsers.clear();
			maxUsers.push_back(user);
			countMax = user.checkMax();
		}
	if (maxUsers.size() == 1)
		return maxUsers.front();
	else
	{
		HumanPlayer playerMin = users.front();
		for (auto&& user : users)
			if (user.getAge() < playerMin.getAge())
				playerMin = user;
		return playerMin;
	}
}

void Game::play(std::istream& in, std::ostream& os)
{
	std::vector<HumanPlayer> users;
	Board board;
	UnusedPieces bag;

	os << board << "\n\n\n";
	//Number of players playing
	os << "How many players are playing?\n";
	uint16_t numberOfPlayers;
	do
	{
		in >> numberOfPlayers;
		if (numberOfPlayers < 2 || numberOfPlayers > 4)
		{
			os << "Invalid number of players! Enter a number between 2 and 4:\n";
		}
	} while (numberOfPlayers < 2 || numberOfPlayers > 4);

	uint16_t numberOfPlayersAux = numberOfPlayers;
	uint16_t userID = 0;
	//Getting the player's name and age
	while (numberOfPlayersAux > 0)
	{
		std::string name;
		uint16_t age;
		os << "Type in the player's name: ";
		in >> name;
		os << "Type in the player's age: ";
		in >> age;
		HumanPlayer player(userID, name, age, bag);
		userID++;
		users.push_back(player);
		numberOfPlayersAux--;
	}

	//The youngest player
	if (users.size() > 1)
	{
		HumanPlayer firstPlayer = whoIsFirst(users);
		cout << "\nThe player that starts is " << firstPlayer.getName() << ".\nFirst piece gets placed in the middle.\n";
		std::swap(users[0], users[firstPlayer.getId()]);
	}

	//Main game, taking turns, taking in the player's options
	int userTurn = 0;  //the user whose turn is
	while (!bag.isEmpty())
	{
		os << "\nPlayer options: \n0 - End turn;\n1 - Place a piece;\n2 - Exchange pieces;\n ";
		os << "\nIt's " << users[userTurn].getName() << "'s turn.\n";
		PlayerView::showScore(os, users[userTurn]); os << endl;
		os << "\nPieces in hand: ";
		PlayerView::showPieces(os, users[userTurn]);
		int userChoice;
		os << "\nSelect an option: ";
		in >> userChoice;
		switch (userChoice)
		{
		case 0:
		{
			if (userTurn == numberOfPlayers - 1)
				userTurn = 0;
			else
				userTurn++;
			std::cout << board << std::endl;
			break;
		}
		case 1:
		{
			os << "\nPieces in hand: ";
			PlayerView::showPieces(os, users[userTurn]);
			os << "\nInsert positions (line, column): ";
			uint16_t i, j;
			in >> i >> j;
			uint16_t pieceID;
			os << "Which piece do you want to place? ";
			in >> pieceID;
			if (users[userTurn].PlacePiece(users[userTurn].getPiece(pieceID), board, std::make_pair(i, j)))
				os << "\nSucces!\n";
			else
				os << "\nFailed!\n";
			std::cout << board << std::endl;
			users[userTurn].fillCurrentPieces(bag);
			break;
		}
		case 2:
		{
			os << "\nPieces in hand: ";
			PlayerView::showPieces(os, users[userTurn]);
			std::vector<uint16_t> piecesIds;
			os << "\nHow many pieces do you want to change? ";
			uint16_t no;
			in >> no;
			os << "\nChoose what pieces to change: ";
			for (uint16_t index = 0; index < no; index++)
			{
				uint16_t x;
				in >> x;
				piecesIds.push_back(x);
			}
			users[userTurn].changePieces(piecesIds, bag);
			break;
		}
		default:
		{
			os << "\nInvalid option!\n";
			break;
		}
		}


	}
}